#pragma once
#include "Collider.h"

class CubeCollider: public Collider {
private:
    Vector3 position;
    Vector3 rotation;
    float halfWidth;


    inline CubeCollider(Vector3 position, Vector3 rotation, float width):
        position{position},
        rotation{rotation},
        halfWidth{width/2} {}
public:
    inline CubeCollider(Vector3 position, float width): CubeCollider(position, {}, width) {}

    virtual void collide(Vector3 *positions, Vector3 *velocities, size_t count) override;
};
