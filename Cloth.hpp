#pragma once

#include "ParticleSystem.h"
#include "Spring.hpp"

class Cloth: public ParticleSystem {
private:
    std::vector<Spring*> springs;
    float interParticleDistance;

    void addSpring(Spring*);
public:
    Cloth(Vector3 centre, int width, int height, float interParticleDistance);
    inline Cloth(Vector3 centre, int width): Cloth(centre, width, width, 0.5f) {}
    inline Cloth(Vector3 centre, int width, float interParticleDistance): Cloth(centre, width, width, interParticleDistance) {}

    ~Cloth();

    virtual void draw(Matrix matModel) {
        ParticleSystem::draw(matModel);
        for (auto spring: springs) {
            spring->draw(positions, matModel);
        }
    }
};
