#include "Ball.hpp"
#include "vector3maths.hpp"

void Ball::collide(Vector3 *positions, Vector3 *velocities, size_t count) {
    for (size_t i = 0; i<count; i++) {
        auto pos = positions[i];
        auto distanceVec = pos - position;

        auto distance = Vector3Length(distanceVec);
        if (distance <= radius) {
            // remove accelerations
            velocities[i] = {0, 0, 0};

            // replace it on the surface
            auto normal = Vector3Normalize(distanceVec);
            auto newVec = normal * radius;
            positions[i] = position + newVec;
        }
    }
}
