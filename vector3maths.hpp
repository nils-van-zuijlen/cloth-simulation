#pragma once

#include <raylib.h>
#include <raymath.h>
#include <iostream>

static inline Vector3 operator+(Vector3 a, Vector3 b) {
    return Vector3Add(a, b);
}

static inline Vector3 operator*(Vector3 vec, float scalar) {
    return Vector3Scale(vec, scalar);
}

static inline Vector3& operator*=(Vector3 &vec, float scalar) {
    vec = vec*scalar;
    return vec;
}

static inline bool operator==(Vector3 a, Vector3 b) {
    return a.x == b.x && a.y == b.y && a.z == b.z;
}

static inline bool operator!=(Vector3 a, Vector3 b) {
    return !(a == b);
}

static inline std::ostream & operator<<(std::ostream &output, const Vector3 &v) {
    output << "{" << v.x << "," << v.y << "," << v.z << "}";
    return output;
}

static inline Vector3 operator-(Vector3 a, Vector3 b) {
    return Vector3Subtract(a, b);
}
