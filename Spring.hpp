#pragma once

#include "Force.h"

class Spring: public Force {
private:
    size_t from, to;
    float initialSize;

    float k = 10000.f;
    float a = 0.01f;
public:
    Spring(size_t from, size_t to, float initialSize);
    virtual ~Spring()=default;

    virtual void apply(Vector3 *positions, Vector3 *velocities, size_t count, float dt) override;

    void draw(Vector3 *positions, Matrix matModel);
};
