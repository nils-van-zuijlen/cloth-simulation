#include "Spring.hpp"
#include "vector3maths.hpp"

Spring::Spring(size_t from, size_t to, float initialSize):from{from}, to{to}, initialSize{initialSize} {}

void Spring::apply(Vector3 *positions, Vector3 *velocities, size_t count, float dt) {
    auto fromP = positions[from], toP = positions[to];
    auto vec = fromP - toP;

    auto distance = Vector3Length(vec);
    auto normal = vec * (1/distance);

    auto sizeDiff = initialSize - distance;
    auto force = normal * (k/2) * -sizeDiff;

    force *= a;
    force *= dt;

    velocities[to] = velocities[to] + force;
    velocities[from] = velocities[from] - force;
}

void Spring::draw(Vector3 *positions, Matrix matModel) {
    DrawLine3D(positions[from], positions[to], BLUE);
}
