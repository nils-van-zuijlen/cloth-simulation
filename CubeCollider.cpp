#include "CubeCollider.hpp"
#include "vector3maths.hpp"

void CubeCollider::collide(Vector3 *positions, Vector3 *velocities, size_t count) {
    for (size_t i = 0; i<count; i++) {
        auto pos = positions[i];
        auto distanceVec = pos - position;
        auto in = false;

        if (
                -halfWidth < distanceVec.x and distanceVec.x < halfWidth
            and -halfWidth < distanceVec.y and distanceVec.y < halfWidth
            and -halfWidth < distanceVec.z and distanceVec.z < halfWidth
        ) {
            // x, -x, y, -y, z, -z
            float dist[6] {distanceVec.x, -distanceVec.x, distanceVec.y, -distanceVec.y, distanceVec.z, -distanceVec.z};
            size_t max = 0;
            for (size_t j = 0; j<6; j++) {
                if (dist[j]>dist[max]) max = j;
            }
            switch (max) {
                case 0:
                    distanceVec.x = halfWidth;
                    velocities[i].x = 0;
                    break;
                case 1:
                    distanceVec.x = -halfWidth;
                    velocities[i].x = 0;
                    break;
                case 2:
                    distanceVec.y = halfWidth;
                    velocities[i].y = 0;
                    break;
                case 3:
                    distanceVec.y = -halfWidth;
                    velocities[i].y = 0;
                    break;
                case 4:
                    distanceVec.z = halfWidth;
                    velocities[i].z = 0;
                    break;
                case 5:
                    distanceVec.z = -halfWidth;
                    velocities[i].z = 0;
                    break;
            }

            positions[i] = position + distanceVec;
        }
    }
}
