# GNU Make workspace makefile autogenerated by Premake

ifndef config
  config=debug_x64
endif

ifndef verbose
  SILENT = @
endif

ifeq ($(config),debug_x64)
  raylib_config = debug_x64
  App_config = debug_x64
endif
ifeq ($(config),debug.dll_x64)
  raylib_config = debug.dll_x64
  App_config = debug.dll_x64
endif
ifeq ($(config),release_x64)
  raylib_config = release_x64
  App_config = release_x64
endif
ifeq ($(config),release.dll_x64)
  raylib_config = release.dll_x64
  App_config = release.dll_x64
endif

PROJECTS := raylib App

.PHONY: all clean help $(PROJECTS) 

all: $(PROJECTS)

raylib:
ifneq (,$(raylib_config))
	@echo "==== Building raylib ($(raylib_config)) ===="
	@${MAKE} --no-print-directory -C build/raylib -f Makefile config=$(raylib_config)
endif

App: raylib
ifneq (,$(App_config))
	@echo "==== Building App ($(App_config)) ===="
	@${MAKE} --no-print-directory -C build/App -f Makefile config=$(App_config)
endif

clean:
	@${MAKE} --no-print-directory -C build/raylib -f Makefile clean
	@${MAKE} --no-print-directory -C build/App -f Makefile clean

help:
	@echo "Usage: make [config=name] [target]"
	@echo ""
	@echo "CONFIGURATIONS:"
	@echo "  debug_x64"
	@echo "  debug.dll_x64"
	@echo "  release_x64"
	@echo "  release.dll_x64"
	@echo ""
	@echo "TARGETS:"
	@echo "   all (default)"
	@echo "   clean"
	@echo "   raylib"
	@echo "   App"
	@echo ""
	@echo "For more information, see https://github.com/premake/premake-core/wiki"
