#pragma once
#include "Collider.h"

class Ball: public Collider {
private:
    Vector3 position;
    float radius;

public:
    inline Ball(Vector3 position, float radius): position{position}, radius{radius} {}

    virtual void collide(Vector3 *positions, Vector3 *velocities, size_t count) override;
};
