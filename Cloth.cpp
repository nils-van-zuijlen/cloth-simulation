#include "Cloth.hpp"

Cloth::Cloth(Vector3 centre, int width, int height, float interParticleDistance): ParticleSystem(width*height), interParticleDistance{interParticleDistance} {
    auto startX = centre.x - ((interParticleDistance * float(width-1)) / 2),
         startY = centre.y,
         startZ = centre.z - ((interParticleDistance * float(height-1)) / 2);

    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            auto n = i*height + j;

            velocities[n] = {0, 0, 0};
            positions[n] = {
                float(i)*interParticleDistance + startX,
                startY,
                float(j)*interParticleDistance + startZ
            };
        }
    }

    const auto sqrt2 = sqrt(2);

    // orthogonal, 1 len springs
    for (int i = 0; i < (width-1); i++) {
        for (int j = 0; j < (height-1); j++) {
            auto sh = new Spring(i*height + j, (i+1)*height + j, interParticleDistance);
            addSpring(sh);
            auto sv = new Spring(i*height + j, i*height + j+1, interParticleDistance);
            addSpring(sv);

            // diagonal
            auto sd = new Spring(i*height + j, (i+1)*height + j+1, interParticleDistance * sqrt2);
            addSpring(sd);
        }
        auto lastHs = new Spring(i*height + (height-1), (i+1)*height + (height-1), interParticleDistance);
        addSpring(lastHs);
    }
    for (int j=0; j < (height-1); j++) {
        auto lastVs = new Spring((width-1)*height + j, (width-1)*height + j+1, interParticleDistance);
        addSpring(lastVs);
    }


    // other diagonal
    for (int i = 1; i < width; i++) {
        for (int j = 0; j < (height-1); j++) {
            auto sd = new Spring(i*height + j, (i-1)*height + j+1, interParticleDistance * sqrt2);
            addSpring(sd);
        }
    }

    // orthogonal, 2 len springs
    for (int i = 0; i < (width-2); i++) {
        for (int j = 0; j < (height-2); j++) {
            auto sh = new Spring(i*height + j, (i+2)*height + j, 2*interParticleDistance);
            addSpring(sh);
            auto sv = new Spring(i*height + j, i*height + j+2, 2*interParticleDistance);
            addSpring(sv);
        }
        auto lastHs = new Spring(i*height + (height-1), (i+2)*height + (height-1), 2*interParticleDistance);
        addSpring(lastHs);
    }
    for (int j=0; j < (height-2); j++) {
        auto lastVs = new Spring((width-1)*height + j, (width-1)*height + j+2, 2*interParticleDistance);
        addSpring(lastVs);
    }
}

void Cloth::addSpring(Spring* spring) {
    springs.emplace_back(spring);
    addForce(spring);
}

Cloth::~Cloth() {
    for (auto spring: springs) {
        delete spring;
    }
}
